from textwrap import fill
from tkinter import *
from tkinter import ttk

# Create root widget and config
root = Tk()
root.title("Student Table")
root.geometry('1200x680')
root.resizable(False,False)

columns = ('first_name', 'last_name','email','major')
tree = ttk.Treeview(root, columns=columns , show="headings",height=100)

# create column
tree.column('first_name',width=500 , anchor='c',stretch=NO)
tree.column('last_name',width=500 , anchor='c',stretch=NO)
tree.column('email', width=500 , anchor='c',stretch=NO)
tree.column('major', width=500 , anchor='c',stretch=NO)

# create heading
tree.heading('first_name', text="First Name")
tree.heading('last_name', text="Last Name")
tree.heading('email', text="Email")
tree.heading('major', text="Major")

# insert data
tree.insert('',END,values=('ChansoThearith','Houl','Thearithhoul@gmail.com','MIS'))
tree.insert('',END,values=('Rothpanhsak','Im','RothpanhsakIm@gmail.com','MIS'))
tree.insert('',END,values=('Chhe','Kev','CheeKev@gmail.com','MIS'))

#display 
tree.pack(fill=BOTH)
root.state('zoomed')
root.mainloop()